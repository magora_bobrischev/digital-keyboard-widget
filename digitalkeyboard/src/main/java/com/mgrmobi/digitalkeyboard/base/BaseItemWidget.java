package com.mgrmobi.digitalkeyboard.base;

import android.support.annotation.NonNull;

import com.mgrmobi.digitalkeyboard.DefaultKey;

/**
 * Created by Serega on 24.08.2015.
 */
public interface BaseItemWidget {

    void bindKeyItem(@NonNull DefaultKey key);

    void swapData();
}
