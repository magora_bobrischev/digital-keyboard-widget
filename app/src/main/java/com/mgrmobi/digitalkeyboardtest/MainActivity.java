package com.mgrmobi.digitalkeyboardtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.mgrmobi.digitalkeyboard.DefaultItemsGenerator;
import com.mgrmobi.digitalkeyboard.DefaultKey;
import com.mgrmobi.digitalkeyboard.DigitalKeyboard;
import com.mgrmobi.digitalkeyboard.DefaultKeyboardAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final DigitalKeyboard keyboard = (DigitalKeyboard) findViewById(R.id.keyboard);
        keyboard.initDefault(new DefaultKeyboardAdapter.OnKeyClickListener() {
            @Override
            public void onItemClicked(DefaultKey key) {
                switch (key.getActualKeyCode()){
                    case DefaultItemsGenerator.KEY_CHANGE_LAYOUT:
                        keyboard.swapLayout();
                        break;

                    case DefaultItemsGenerator.KEY_BACKSPACE:
                        Toast.makeText(MainActivity.this, "Click BACKSPACE", Toast.LENGTH_SHORT).show();
                        break;

                    case DefaultItemsGenerator.KEY_PAUSE:
                        Toast.makeText(MainActivity.this, "Click PAUSE", Toast.LENGTH_SHORT).show();
                        break;

                    case DefaultItemsGenerator.KEY_WAIT:
                        Toast.makeText(MainActivity.this, "Click WAIT", Toast.LENGTH_SHORT).show();
                        break;

                    case DefaultItemsGenerator.KEY_STAR:
                        Toast.makeText(MainActivity.this, "Click *", Toast.LENGTH_SHORT).show();
                        break;

                    case DefaultItemsGenerator.KEY_DASH:
                        Toast.makeText(MainActivity.this, "Click #", Toast.LENGTH_SHORT).show();
                        break;

                    case DefaultItemsGenerator.KEY_PLUS:
                        Toast.makeText(MainActivity.this, "Click +", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                        Toast.makeText(MainActivity.this, "Click " + key.getActualSymbol(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        ((DefaultKeyboardAdapter) keyboard.getAdapter()).setUseRipple(true);

        /*
            OR:
            keyboard.setAdapter(...Some custom adapter extend BaseAdapter...)
         */
    }
}
